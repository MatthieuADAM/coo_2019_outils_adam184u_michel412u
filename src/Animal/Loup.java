package Animal;

/**
 * 
 * @author Matthieu
 * Classe Loup
 */
public class Loup implements Animal{
	private int pv;
	private int StockNourriture;
	
	/**
	 * Constructeur parametre
	 * @param pv
	 * @param nourriture
	 */
	public Loup(int pv, int nourriture) {
		if(pv < 0 || nourriture <0) {
			this.pv = 10;
			this.StockNourriture = 10;
		} else {
		this.pv = pv;
		this.StockNourriture = nourriture;
		}
	}
	
	/**
	 * Constructeur vide
	 */
	public Loup() {
		this.pv = 10;
		this.StockNourriture = 10;
	}	
	
	/**
	 * Stocke de la nourriture
	 * @param nourriture Quantite de nourriture engrangee par l'animal
	 */
	@Override
	public void stockerNourriture(int nourriture) {
		this.StockNourriture = this.StockNourriture + nourriture;
		if(this.StockNourriture < 0) {
			this.StockNourriture = 0;
		}
	}
	
	/**
	 * Donne le nombre de pv
	 * @return le nombre de pv de l'animal
	 */
	@Override
	public int getPv() {
		return this.pv;
	}
	
	/**
	 * Donne le stock de nourriture
	 * @return Le stock de nourriture de l'animal
	 */
	@Override
	public int getStockNourriture() {
		return this.StockNourriture;
	}

	/**
	 * Dit si l'animal est mort
	 * @return L'etat de l'animal
	 */
	@Override
	public boolean etreMort() {
		int pointVie = this.getPv();
		if(pointVie <= 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Fait passer un jour a l'animal
	 * @return Si le jour a ete passe
	 */
	@Override
	public boolean passerUnjour() {
		if(!this.etreMort()) {
			if(this.getStockNourriture() > 0) {
				this.StockNourriture = this.StockNourriture - 1;
			} else {
				this.pv = this.pv-1;
			}
			if(this.pv > 0) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Loup [pv=" + pv + ", StockNourriture=" + StockNourriture + "]";
	}
}
