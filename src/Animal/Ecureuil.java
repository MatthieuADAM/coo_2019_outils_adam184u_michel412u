package Animal;

public class Ecureuil implements Animal{

	private int pv,stockNourriture;
	
	public Ecureuil() {
		this.pv = 10;
		this.stockNourriture = 10;
	}
	
	public Ecureuil(int pv,int stockNourriture) {
		if(pv >= 0) {
			this.pv = pv;
		}else {
			this.pv = 10;
		}
		
		if(stockNourriture >= 0) {
			this.stockNourriture = stockNourriture;
		}else {
			this.stockNourriture = 10;
		}
	}
	
	@Override
	public boolean etreMort() {
		return this.pv == 0;
	}

	@Override
	public boolean passerUnjour() {
		boolean retour=false;
		if(!this.etreMort()) {
			if(this.getStockNourriture()>0) {
				retour = true;
				this.stockerNourriture(-1);
			}else {
				retour = true;
				this.pv--;
			}
		}	
		return retour;
	}

	@Override
	public void stockerNourriture(int nourriture) {
		if(!this.etreMort()) {
			if(this.stockNourriture+nourriture>=0) {
				this.stockNourriture+=nourriture;
			}else {
				this.stockNourriture = 0;
			}
		}
	}

	@Override
	public int getPv() {
		return this.pv;
	}

	@Override
	public int getStockNourriture() {
		return this.stockNourriture;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Ecureuil [pv=" + pv + ", stockNourriture=" + stockNourriture + "]";
	}

}
