package Tests;

import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;

import Animal.Ecureuil;

/**
 * @author Matthieu
 *
 */
class TestEcureuil {


	@Test
	/**
	 * methode de test constructeur parametre
	 */
	void testConstructeur() {
		//Cr�ation d'objet
		Ecureuil Damien = new Ecureuil(-10000, 25000000);
		//Test
		assertEquals("L'ecureuil doit avoir 10 pv",10,Damien.getPv());
		assertEquals("L'ecureuil doit avoir 25000000 de nourriture",25000000,Damien.getStockNourriture());
	}
	
	@Test
	/**
	 * methode de test constructeur vide
	 */
	void testConstructeurVide() {
		//Cr�ation d'objet
				Ecureuil Damien = new Ecureuil();
				//Test
				assertEquals("L'ecureuil doit avoir 10 pv",10,Damien.getPv());
				assertEquals("L'ecureuil doit avoir 10 de nourriture",10,Damien.getStockNourriture());
	}
	
	@Test
	/**
	 * methode de test du stock
	 */
	void testStockNourriture() {
		//Cr�ation d'objet
			Ecureuil Damien = new Ecureuil();
		//Execution methode
			Damien.stockerNourriture(5);
		//Test
			assertEquals("L'ecureuil doit avoir 15 de nourriture",15,Damien.getStockNourriture());
	}
	
	@Test
	/**
	 * methode de test du stock a 0
	 */
	void testStockNourritureZero() {
		//Cr�ation d'objet
			Ecureuil Damien = new Ecureuil();
		//Execution methode
			Damien.stockerNourriture(0);
		//Test
			assertEquals("L'ecureuil doit avoir 10 de nourriture",10,Damien.getStockNourriture());
	}
	
	@Test
	/**
	 * methode de test du stock inf a 0
	 */
	void testStockNourritureInf() {
		//Cr�ation d'objet
			Ecureuil Damien = new Ecureuil(2,0);
		//Execution methode
			Damien.stockerNourriture(-5);
		//Test
			assertEquals("L'ecureuil doit avoir 0 de nourriture",0,Damien.getStockNourriture());
	}
	
	@Test
	/**
	 * methode de test du stock animal mort
	 */
	void testStockNourritureMort() {
		//Cr�ation d'objet
			Ecureuil Damien = new Ecureuil(0,0);
		//Execution methode
			Damien.stockerNourriture(-5);
		//Test
			assertEquals("L'ecureuil doit avoir 0 de nourriture",0,Damien.getStockNourriture());
	}
	
	@Test
	/**
	 * methode de test de la mort
	 */
	void testEtreMort() {
		//Cr�ation d'objet
			Ecureuil Damien = new Ecureuil(1,0);
			assertEquals("L'ecureuil doit �tre vivant",false,Damien.etreMort());
		//Execution methode
			Damien.passerUnjour();
		//Test
			assertEquals("L'ecureuil doit �tre mort",true,Damien.etreMort());
	}
	
	@Test
	/**
	 * methode de test passer un jour
	 */
	void testPasserUnJour() {
		//Creation d'objet
			Ecureuil e = new Ecureuil();
		//Execution methode
			e.passerUnjour();
		//Test
			assertEquals("L'ecureuil devrait avoir 10 pv",10,e.getPv());
			assertEquals("L'ecureuil devrait avoir 9 nourriture",9,e.getStockNourriture());
	}
	
	@Test
	/**
	 * methode de test passer un jour sans nourriture
	 */
	void testPasserUnJourZeroNourriture() {
		//Creation d'objet
			Ecureuil e = new Ecureuil(10,0);
		//Execution methode
			e.passerUnjour();
		//Test
			assertEquals("L'ecureuil devrait avoir 9 pv",9,e.getPv());
			assertEquals("L'ecureuil devrait avoir 0 nourriture",0,e.getStockNourriture());
	}
	
	@Test
	/**
	 * methode de test passer un jour mort
	 */
	void testPasserUnJourMort() {
		//Creation d'objet
			Ecureuil e = new Ecureuil(0,0);
		//Execution methode
			e.passerUnjour();
		//Test
			assertEquals("L'ecureuil devrait avoir 0 pv",0,e.getPv());
			assertEquals("L'ecureuil devrait avoir 0 nourriture",0,e.getStockNourriture());
	}
}
