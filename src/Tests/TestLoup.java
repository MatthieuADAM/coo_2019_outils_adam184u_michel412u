package Tests;

import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;

import Animal.*;

class TestLoup {

	
	@Test
	/**
	 * test du constructeur vide
	 */
	void testConstructeurVide() {
		//methode testee
		Loup l = new Loup();
		
		//verification du resultat
		assertEquals("le loup devrait avoir 10 points de vie",10,l.getPv());
		assertEquals("le loup devrait avoir 10 en stock de nourriture",10,l.getStockNourriture());
	}
	
	
	@Test
	/**
	 * test du constructeur complet cas normal
	 */
	void testConstructeurClassique() {
		//methode testee
		Loup l = new Loup(10,10);
		
		//verification du resultat
		assertEquals("le loup devrait avoir 10 points de vie",10,l.getPv());
		assertEquals("le loup devrait avoir 10 en stock de nourriture",10,l.getStockNourriture());
	}
	
	
	@Test
	/**
	 * test du constructeur complet avec des valeurs négatives 
	 */
	void testConstructeurValeursNegatives() {
		//methode testee
		Loup l = new Loup(-10,-10);
		
		//verification du resultat
		assertEquals("le loup devrait avoir des points de vie egaux a 10",10,l.getPv());
		assertEquals("le loup devrait avoir un stock de nourriture egaux a 10",10,l.getStockNourriture());
	}
	
	
	@Test
	/**
	 * test etre mort cas loup vivant, et mort
	 */
	void testEtreMort() {
		//preparation des donnees
		Loup l = new Loup(1,0);
		
		//methode testee et verification du resultat
		assertEquals("le loup devrait etre vivant",false,l.etreMort());
		l.passerUnjour();
		assertEquals("le loup devrait etre mort",true,l.etreMort());
		l.passerUnjour();
		assertEquals("le loup devrait etre mort",true,l.etreMort());
	}
	
	
	@Test
	/**
	 * test stocker nourriture cas normal
	 */
	void testStockerNourriture() {
		//preparation des donnees
		Loup l = new Loup();
		
		//methode testee
		l.stockerNourriture(1);
		
		//verification du resultat
		assertEquals("le loup devrait avoir 11 de nourriture",11,l.getStockNourriture());
	}
	
	
	@Test
	/**
	 * test stocker nourriture cas ou l'on retire de la nourriture
	 */
	void testStockerNourritureNegatif() {
		//preparation des donnees
		Loup l = new Loup();
		
		//methode testee
		l.stockerNourriture(-1);
		
		//verification du resultat
		assertEquals("le loup devrait avoir 9 de nourriture",9,l.getStockNourriture());
	}
	
	
	@Test
	/**
	 * test stocker nourriture cas ou l'on retire plus de nourriture que disponible
	 */
	void testStockerNourritureSupAuStock() {
		//preparation des donnees
		Loup l = new Loup(1,1);
		
		//methode testee
		l.stockerNourriture(-5);
		
		//verification du resultat
		assertEquals("le loup devrait avoir 0 de nourriture",0,l.getStockNourriture());
	}
	
	
	@Test
	/**
	 * test stocker nourriture cas ou le loup est mort
	 */
	void testStockerNourritureLoupMort() {
		//preparation des donnees
		Loup l = new Loup(0,0);
		
		//methode testee
		l.stockerNourriture(1);
		
		//verification du resultat
		assertEquals("le loup ne devrait pas pouvoir stocker de nourriture",0,l.getStockNourriture());
	}
	
	
	@Test 
	/**
	 * test passer un jour cas normal
	 */
	void testPasserUnJour() {
		//preparation des donnees
		Loup l = new Loup();
		
		//methode testee et verification du resultat
		assertEquals("le loup devrait passer le jour",true,l.passerUnjour());
		assertEquals("le loup devrait avoir 10 pv",10,l.getPv());
		assertEquals("le loup devrait avoir 9 de nourriture",9,l.getStockNourriture());
	}
	
	
	@Test 
	/**
	 * test passer un jour sans nourriture
	 */
	void testPasserUnJourSansNourriture() {
		//preparation des donnees
		Loup l = new Loup(10,0);
		
		//methode testee et verification du resultat
		assertEquals("le loup devrait passer le jour",true,l.passerUnjour());
		assertEquals("le loup devrait avoir 9 pv",9,l.getPv());
		assertEquals("le loup devrait avoir 0 de nourriture",0,l.getStockNourriture());
	}
	
	
	@Test 
	/**
	 * test passer un jour lorsque le loup est deja mort
	 */
	void testPasserUnJourLoupMort() {
		//preparation des donnees
		Loup l = new Loup(0,0);
		
		//methode testee et verification du resultat
		assertEquals("le loup ne devrait pas passer un jour",false,l.passerUnjour());
		assertEquals("le loup devrait avoir 0 pv",0,l.getPv());
		assertEquals("le loup devrait avoir 0 de nourriture",0,l.getStockNourriture());
	}
}
